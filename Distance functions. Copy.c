#include<stdio.h>
#include<math.h>
float distance(float a,float b,float c,float d);
void output(float e,float f,float g,float h,float j);
float input()
{	
	float x;
	scanf("%f",&x);
	return x;
}
float main()
{
	float p,q,r,s,final;
	printf("Enter the x coordinate of the first point=");
	p=input();
	printf("Enter the x coordinate of the second point=");
	q=input();
	printf("Enter the y coordinate of the first point=");
	r=input();
	printf("Enter the y coordinate of the second point=");
	s=input();
	final=distance(p,q,r,s);
	output(p,q,r,s,final);
}
float distance(float a,float b,float c,float d)
{
	float total, sum1, sum2;
	sum1=a-b;
	sum2=c-d;
	total=sqrt(sum1*sum1+sum2*sum2);
	return total;
}
void output(float e,float f,float g,float h,float j)
{
	printf("The entered x coordinates of the point x1=%f\tx2=%f\n",e,f);
	printf("The entered y coordinates of the point y1=%f\ty2=%f\n",g,h);
	printf("The distance between the given two points=%f\n",j);
}